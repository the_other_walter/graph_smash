#!/usr/bin/python
import glob
import os.path
from board import board
from play_game import play_game

settings_file_name = ".settings"

print ("\
   ____ ____      _    ____  _   _   ____  __  __    _    ____  _   _ _ \n\
  / ___|  _ \    / \  |  _ \| | | | / ___||  \/  |  / \  / ___|| | | | |\n\
 | |  _| |_) |  / _ \ | |_) | |_| | \___ \| |\/| | / _ \ \___ \| |_| | |\n\
 | |_| |  _ <  / ___ \|  __/|  _  |  ___) | |  | |/ ___ \ ___) |  _  |_|\n\
  \____|_| \_\/_/   \_\_|   |_| |_| |____/|_|  |_/_/   \_\____/|_| |_(_)\n\
  ")
print "Happy Birthday Jacob! -From Walter"

# Create intro screen.
intro_board = board()
intro_board.add_space("GRAPH SMASH!")
intro_board.spaces["GRAPH SMASH!"].owner = "ff0000"
intro_board.add_space("More Graphs!\nMore Smash!\nMore Exclamation Point!")
intro_board.spaces["More Graphs!\nMore Smash!\nMore Exclamation Point!"].owner = "00ff00"
intro_board.add_space("Happy Birthday Jacob!")
intro_board.spaces["Happy Birthday Jacob!"].owner = "ff00ff"
intro_board.add_space("Enter commands\nfrom the terminal!")
intro_board.spaces["Enter commands\nfrom the terminal!"].owner = "00ffff"
intro_board.add_space("Have fun,\nsmash graphs!")
intro_board.spaces["Have fun,\nsmash graphs!"].owner = "ffff00"
intro_board.add_space("From Walter!")
intro_board.spaces["From Walter!"].owner = "0000ff"

intro_board.connect("GRAPH SMASH!", "From Walter!")
intro_board.connect("GRAPH SMASH!", "Happy Birthday Jacob!")
intro_board.connect("GRAPH SMASH!", "Enter commands\nfrom the terminal!")
intro_board.connect("GRAPH SMASH!", "Have fun,\nsmash graphs!")
intro_board.connect("GRAPH SMASH!", "More Graphs!\nMore Smash!\nMore Exclamation Point!")

intro_board.draw_board(True)

# Try to get board to use.
settings_file = file(settings_file_name, 'r')
board_to_use = settings_file.readline().strip()

while True:
    can_play = os.path.isfile(board_to_use)
    if not can_play:
        print "Can't find board '" + board_to_use + "'.  Pick a different one."
    print "Menu Options:"
    print "Enter the number you want to go to."
    print "1) Help"
    print "2) Pick Map"
    if can_play:
        print "3) Play " + board_to_use
    print "4) Quit"
    command = raw_input(">>>")
    if command == "1":
        print "Gameplay:"
        print "You can type in instructions during your turn."
        print "When two units crash into each other, they both explode."
        print "This explosion will destroy the spaces a certain distance"
        print "from the space of collision.  The more units that get caught"
        print "in the explosion, the more the radius increases.  If spaces"
        print "are no longer connected to the pentagon, they will be destroyed."
        print "If units were instructed to go somewhere before before an"
        print "explosion, they will fall into the crater and be destroyed if"
        print "they try to to step into a destroyed space.  This can be"
        print "corrected by instructing them to move again."
        print "End Game:"
        print "If one player loses all units, the game ends.  Whichever team"
        print "has not lost a player wins."
        print "If all units are destroyed, the team whose closest unit to the"
        print "pentagon is the farthest from the pengagon wins."
    elif command == "2":
        print "Maps:"
        for m in sorted(glob.glob("maps/*")):
            print m
        board_to_use = raw_input("Which map would you like?")
        settings_file = file(settings_file_name, 'w')
        settings_file.write(board_to_use)
    elif command == "3" and can_play:
        next_map = play_game(board_to_use)
        if next_map != "":
            board_to_use = next_map
            settings_file = file(settings_file_name, 'w')
            settings_file.write(board_to_use)
    elif command == "4":
        print "Game over man!"
        exit()
    else:
        print "Print please enter one of the numbers of the options."
