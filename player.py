import re
class player:
    def __init__(self, name, color, kind, team):
        '''
        color is hexadecimal color value.
        kind indicates if this is ai or human.
            -user:  human player
            -rand:  does random moves
        '''
        self.name = name
        self.color = color
        self.kind = kind
        self.team = team

    def player_title(self):
        '''
        Returns string that represents the player.
        '''
        return self.name + "(" + str(self.kind) + ", Team " + str(self.team) + ")"

    def move(self,board):
        if self.kind == "user":
            # Request user input.
            while True:
                new_input = raw_input(self.player_title() + ":")
                new_input = re.sub(' +',' ',new_input)
                new_input = new_input.strip()
                if new_input == "wait":
                    break
                if new_input == "screen":
                    board.draw_board(True)
                    continue
                if new_input == "players":
                    print "Players:"
                    for p in board.players:
                        if p != 0:
                            print p.player_title()
                    continue
                if new_input == "resign":
                    for s in board.spaces.keys():
                        if board.spaces[s].owner == self.color:
                            board.spaces[s].owner = ""
                    break
                if new_input.split(" ")[0] == "path":
                    # Figure out target units.
                    target_units = []
                    if len(new_input.split(" ")) == 1:
                        # Assume all units that are moving.
                        for s in board.spaces.keys():
                            if len(board.spaces[s].path) != 0:
                                target_units.append(s)
                    else:
                        error_out = False
                        for i in new_input.split(" ")[1:]:
                            inp = i.strip()
                            # Make sure space exists.
                            if inp not in board.spaces.keys():
                                print "No space '" + inp + "'."
                                error_out = True
                            # Make sure unit is on space.
                            elif board.spaces[inp].owner == "":
                                print "No unit on " + inp + "."
                                error_out = True
                            # Safe to observe this.
                            target_units.append(i.strip())
                        if error_out:
                            continue
                    # Print path.
                    for t in target_units:
                        print "Path of " + board.space_title(t) + ":"
                        for s in board.spaces[t].path:
                            # Make sure that space is present.
                            if s not in board.spaces.keys():
                                print "<--DESTROYED-->"
                                break
                            else:
                                print board.space_title(s)
                    continue
                if new_input.split(" ")[0] == "bfs":
                    # Try to pull in inputs.
                    inps = new_input.split(" ")[1:]
                    if len(inps) != 2:
                        print "bfs needs 2 parameters."
                    else:
                        space_name = inps[0]
                        error_out = False
                        if space_name not in board.spaces.keys():
                            print "'" + space_name + "' is not a space."
                            error_out = True
                        try:
                            radius = int(inps[1])
                            if radius < 0:
                                print "Radius " + str(radius) + " should be non-negative."
                                error_out = True
                        except ValueError:
                            print "'" + inps[1] + "' is not an integer."
                            error_out = True
                        if not error_out:
                            print str(radius) + " around " + space_name + ":"
                            for s in board.bfs(space_name, radius):
                                print board.space_title(s)
                            continue
                if new_input.split(" ")[0] == "units":
                    parameters = new_input.split(" ")[1:]
                    target_players = []
                    error_out = False
                    # If no parameters, assume you want everyone.
                    if len(parameters) == 0:
                        for p in board.players:
                            if p != 0:
                                target_players.append(p.name)
                    # Otherwise, load in mentioned players.
                    else:
                        target_players = parameters
                    # Print out all players.
                    for pa in target_players:
                        units_of_pa = board.units_of(pa)
                        if units_of_pa == 0:
                            print "There is no player '" + pa + "'."
                            continue
                        print "Units of " + board.get_player_of_name(pa).player_title() + ":"
                        for u in units_of_pa:
                            print board.space_title(u)
                    continue
                if "->" in new_input:
                    sides = new_input.split("->")
                    sides[0] = sides[0].strip()
                    sides[1] = sides[1].strip()
                    valid = True
                    # Do both of these spaces exist?
                    for s in sides:
                        if not s in board.spaces.keys():
                            print("There is no space called '" + s + "'.")
                            valid = False
                    # Do you own this piece?
                    if valid and board.spaces[sides[0]].owner != self.color:
                            print("You do not have a unit on " + sides[0])
                            valid = False
                    if valid:
                        board.set_destination(sides[0], sides[1])
                        break
                # If you got here, the command doesn't match anything.
                print("'" + new_input + "' does not work.")
                print("bfs space1 4        - Show all spaces within 4 of space1")
                print("path                - Show the paths of all moving units.")
                print("path space1         - Show the path the unit on space1 will take.")
                print("players             - List players in the game.")
                print("resign              - Your side loses.")
                print("screen              - create a new graph screen.")
                print("space1 -> space2    - Tell your unit on space1 to plot a course for space2.")
                print("units player1       - List units owned by player1.")
                print("wait                - skip your turn.")
        elif self.kind == "still":
            # Stand still.
            pass
        elif self.kind == "computer":
            # AI
            report_string = self.player_title() + ":"
            prospects = board.prospects(self.name, 4, 3)
            if prospects[0] != 0 and prospects[1] != '':
                board.set_destination(prospects[1], prospects[2])
                report_string += prospects[1] + "->" + prospects[2]
            else:
                report_string += "wait"
            print report_string
        else:
            print("Unknown player type '" + self.kind + "'.")
