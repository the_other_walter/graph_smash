import operator
import os
from board import board
def play_game(board_name):
    '''
    Returns the next map to go to.
    '''
    b = board()
    b.read_file(board_name)
    b.update_distance_winner()
    
    current_player = 0;
    while True:
        # Process move.
        if b.current_player() == 0:
            b.advance_units(True)
            w = b.check_win()
            if w != 0:
                b.draw_board(False)
                print "Team " + str(w) + " wins!"
                return b.victory_map[w-1]
            b.update_distance_winner()
            b.draw_board(False)
            # Produce warnings.
            all_that_will_fall = b.all_that_will_fall()
            if len(all_that_will_fall) != 0:
                print "Units are on a course to move onto destroyed spaces!"
                for s in sorted(all_that_will_fall.items(), key=operator.itemgetter(1)):
                    print str(s[1]) + " - " + b.space_title(s[0])
        else:
            b.current_player().move(b)
        b.rotate_players()
