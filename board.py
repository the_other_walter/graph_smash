from space import space
from player import player
import string
import random
import re
import os
class board:
    def __init__(self):
        self.spaces = {}
        self.objective = ""
        self.players = [0] # 0 represents it's time to advance everyone.
        self.distance_winner = 1+int(random.random() > 0.5) # Random start.
        self.victory_map = ["",""]

    def clone(self):
        '''
        Returns a deep duplicate of this board.
        '''
        result = board()
        # Set up space structure.
        for s in self.spaces.keys():
            result.add_space(s)
            result.spaces[s].owner = self.spaces[s].owner
            result.spaces[s].path = self.spaces[s].path[:]
        for s in self.spaces.keys():
            for e in self.spaces[s].edges:
                result.connect(s, e.name)
        # Set up objective.
        result.objective = self.objective
        # Load in players.
        result.players = []
        for p in self.players:
            if p != 0:
                result.players.append(player(p.name, p.color, p.kind, p.team))
            else:
                result.players.append(0)
        # Set up distance winner.
        result.distance_winner = self.distance_winner
        # Set up victory map
        result.victory_map = self.victory_map[:]
        return result

    def add_space(self,space_name):
        if not space_name in self.spaces:
            self.spaces[space_name] = space(space_name)

    def connect(self, space_name_1, space_name_2):
        self.spaces[space_name_1].connect(self.spaces[space_name_2])

    def remove_space(self,space_name):
        # First off, does this node really exist?
        if space_name not in self.spaces.keys():
            return
        # Find edges associated with this node.
        for e in self.spaces[space_name].edges:
            self.spaces[e.name].edges.remove(self.spaces[space_name])
        # Rip out this node.
        del self.spaces[space_name]

    def add_player(self, name, color, kind, team):
        self.players.append(player(name, color, kind, team))

    def rotate_players(self):
        self.players.append(self.players.pop(0))

    def current_player(self):
        return self.players[0]

    def set_destination(self, space_1, space_2):
        '''
        Returns True if destination was set.
        '''
        if space_1 not in self.spaces.keys() or space_2 not in self.spaces.keys():
            return False
        self.spaces[space_1].path = self.find_path(space_1, space_2)[1:]
        return True

    def get_player_of_color(self, color):
        for p in self.players:
            if p != 0 and p.color == color:
                return p
        return 0

    def get_player_of_name(self, name):
        for p in self.players:
            if p != 0 and p.name == name:
                return p
        return 0

    def update_distance_winner(self):
        '''
        The distance winner is the team who is the least close to the
        objective.  This team wins in the case of a draw.
        '''
        # Don't change if objective is gone.
        if self.objective not in self.spaces.keys():
            return
        # Now calculate winner.
        closest_found = {}
        bfs_map = self.bfs(self.objective)
        # Measure each space's distance from the objective.
        for s in self.spaces.keys():
            space_owner = self.spaces[s].owner
            space_owner = self.get_player_of_color(space_owner)
            # Skip empty spaces.
            if space_owner == 0:
                continue
            # Think in terms of teams.
            space_owner = space_owner.team
            # Extract path.
            dist = 0
            last = s
            while last != self.objective:
                last = bfs_map[last]
                dist += 1
            # Is this dist closer than what you have recorded?
            if space_owner not in closest_found.keys() or closest_found[space_owner] > dist:
                closest_found[space_owner] = dist
        # Now find the farthest.
        farthest_distance = -1
        possible_winners = []
        for p in closest_found.keys():
            if closest_found[p] > farthest_distance:
                farthest_distance = closest_found[p]
                possible_winners = [p]
            elif closest_found[p] == farthest_distance:
                possible_winners.append(p)
        # Safety check:  Are there any existing players?
        if len(possible_winners) != 0:
            # Now pick a winner.
            self.distance_winner = random.choice(possible_winners)

    def advance_units(self, print_stuff):
        '''
        Let all units move one space.
        '''
        # Randomly pick the order that units will move.
        spaces = self.spaces.keys()
        random.shuffle(spaces)
        # Marks spaces that peeps have moved onto, as to prevent double-moves.
        do_not_move = set()
        # Move each of them.
        for s in spaces:
            # Ignore if you aren't moving or should not move.
            if s in do_not_move or len(self.spaces[s].path) == 0:
                continue
            next_space = self.spaces[s].path[0]
            # Ignore units that are skipping this turn.
            if next_space == "":
                self.spaces[s].path = self.spaces[s].path[1:]
                continue
            # If this space is gone, fall off world.
            if next_space not in self.spaces.keys():
                if print_stuff:
                    print "The unit that was on " + s + " tried to move onto " + next_space + " and was destroyed."
                self.spaces[s].path = []
                self.spaces[s].owner = ""
                continue
            # Check for collision.
            if self.spaces[next_space].owner != "":
                if print_stuff:
                    # Collision is followed by explosion
                    print "A collision on " + self.space_title(next_space) +" destroyed the following spaces:"
                    # Report the two colliders.
                    print self.space_title(next_space)
                    print self.space_title(s)
                # Remove the two colliders from the equation.
                self.spaces[s].path = []
                self.spaces[s].owner = ""
                self.spaces[next_space].path = []
                self.spaces[next_space].owner = ""
                # Until explosion range stablizes...
                explosion_size = 1
                changed_size = True
                while changed_size:
                    changed_size = False
                    # Count items in explosion range.
                    item_inventory = self.bfs(next_space, explosion_size)
                    new_explosion_size = 1
                    for item in item_inventory.keys():
                        if self.spaces[item].owner != "":
                            new_explosion_size += 1
                    # Explosion range is equal to number of items.
                    if new_explosion_size != explosion_size:
                        changed_size = True
                        explosion_size = new_explosion_size
                # Destroy all spaces in explosion.
                destroy_these = self.bfs(next_space, explosion_size).keys()
                for destroy_this in destroy_these:
                    if print_stuff and destroy_this != s and destroy_this != next_space:
                        print self.space_title(destroy_this)
                    do_not_move.add(destroy_this)
                    self.remove_space(destroy_this)
                removed_islands = self.remove_islands(print_stuff)
                do_not_move.update(removed_islands)
                destroy_these.extend(removed_islands)
            # If you didn't hit anything, move to the location.
            else:
                self.spaces[next_space].path = self.spaces[s].path[1:]
                self.spaces[next_space].owner = self.spaces[s].owner
                do_not_move.add(next_space)
                # You are no longer where you were before.
                self.spaces[s].path = []
                self.spaces[s].owner = ""

    def read_file(self, file_name):
        random_characters = ["1", "2", "3", "4", "7", "f", "h", "j", "k", "p", "w"]
        placing_player = ""
        f = open(file_name, "r")
        for command in f:
            command = re.sub(' +',' ',command).strip()
            if len(command) == 0:
                continue 
            try:
                # Accept space.
                if command[0] == "s":
                    self.add_space(command[1:].strip())
                # Accept objective.
                elif command[0] == "o":
                    self.objective = command[1:].strip()
                    self.add_space(self.objective)
                # Accept travel instructions.
                elif command[0] == "t":
                    params = command[1:].split(",")
                    params = [x.strip() for x in params]
                    self.spaces[params[0]].path = params[1:]
                # Accept edge.
                elif command[0] == "e":
                    sides = command[1:].split("->")
                    self.connect(sides[0].strip(), sides[1].strip())
                # Accept print message.
                elif command[0] == "m":
                    print ">" + command[1:]
                # Accept random.
                elif command[0] == "r":
                    sides = command[1:].split(",")
                    rand_spaces = int(sides[0])
                    rand_edges = int(sides[1])
                    # Make spaces.
                    for ind in range(rand_spaces):
                        # From http://stackoverflow.com/questions/2257441/random-string-generation-with-upper-case-letters-and-digits-in-python
                        new_name = ""
                        while new_name in self.spaces.keys() or new_name == "":
                            new_name = ''.join(random.choice(random_characters) for _ in range(len(new_name)+1))
                        self.add_space(new_name)

                    for ind in range(rand_edges):
                        space_1 = random.choice(self.spaces.keys())
                        space_2 = random.choice(self.spaces.keys())
                        self.connect(space_1, space_2)
                # Accept new player.
                elif command[0] == "p":
                    placing_player = command[1:].strip()
                # Accept unit.
                elif command[0] == "u":
                    self.spaces[command[1:].strip()].owner = placing_player
                # Accept victory map
                elif command[0] == "v":
                    tokens = command[1:].split(" ")
                    self.victory_map[int(tokens[0])-1] = tokens[1]
                # Accept random deployment.
                elif command[0] == "d":
                    rand_units = int(command[1:].strip())
                    for ind in range(rand_units):
                        space = ""
                        while space == "" or self.spaces[space].owner != "":
                            space = random.choice(self.spaces.keys())
                        self.spaces[space].owner = placing_player
                # Accept giving color to a player.
                elif command[0] == "g":
                    sides = command[1:].split("->")
                    color = sides[0]
                    sides = sides[1].split(",")
                    self.players.append(player(sides[0].strip(), color.strip(), sides[1].strip(), int(sides[2].strip())))
                else:
                    print "Not sure how to interpret:"
                    print command
            except:
                print "Error on map file line:"
                print command
        f.close()
        # Get rid of islands.
        self.remove_islands(False)

    def draw_board(self,new_window):
        screen_file_name = "screen_file.dot"
        screen_file = open(screen_file_name, "w")
        screen_file.write("graph g {\n")
        temp_names = {}
        i = 0
        for s in self.spaces.keys():
            if not s in temp_names:
                temp_names[s] = "x" + str(i)
                i += 1
            for e in self.spaces[s].edges:
                e = e.name
                if not e in temp_names:
                    temp_names[e] = "x" + str(i)
                    i += 1
                screen_file.write(temp_names[s] + " -- " + temp_names[e] + "\n")
            screen_file.write(temp_names[s] + " [label=\"" + s + "\"]\n")
            # Figure out if a unit is populating here.
            owner = self.spaces[s].owner
            if owner != "":
                screen_file.write(temp_names[s] + " [style=filled]\n")
                screen_file.write(temp_names[s] + " [fillcolor=\"#" + owner + "\"]\n")
        # Give a special shape to objectives.
        if self.objective in temp_names.keys():
            screen_file.write(temp_names[self.objective] + " [shape=\"house\"]\n")
            screen_file.write(temp_names[self.objective] + " [label=\"" + self.objective + "\n(Best Distance: Team " + str(self.distance_winner) + ")\"]\n")
        # Close.
        screen_file.write("}\n")
        screen_file.close()
        if new_window:
            os.system("xdot " + screen_file_name + " &")

    def bfs(self, start, distance = -1):
        # Safety check.
        if start not in self.spaces.keys():
            return 0
        # Do BFS
        from_dict = {start:""}
        queue = [start]
        distance_table = {start:0}
        while queue:
            vertex  = queue.pop(0)
            # Halt if this is on the edge of allowed territory.
            if distance_table[vertex] == distance:
                continue
            edge_list = list(self.spaces[vertex].edges)
            random.shuffle(edge_list)
            for e in edge_list:
                e = e.name
                if e not in from_dict.keys():
                    from_dict[e] = vertex
                    distance_table[e] = distance_table[vertex] + 1
                    queue.append(e)
        return from_dict

    def find_path(self, start, end):
        # Do BFS.
        bfs_map = self.bfs(start)
        # Check if there was a failure.
        if bfs_map == 0:
            return 0
        # If no path was found, return [].
        if end not in bfs_map.keys():
            return []
        # Extract path.
        result = [end]
        while result[0] != start:
            result.insert(0, bfs_map[result[0]])
        return result

    def remove_islands(self, print_destroyed):
        '''
        Destroys spaces that are not linked to the objective.
        '''
        if self.objective in self.spaces.keys():
            # Figure out where you can reach.
            reachable_set = set(self.bfs(self.objective).keys())
        else:
            # Destory all if there is no objective.
            reachable_set = []
        # Remove all you can't reach.
        result = []
        for s in self.spaces.keys():
            if s not in reachable_set:
                if print_destroyed:
                    print self.space_title(s)
                self.remove_space(s)
                result.append(s)
        return result

    def time_until_fall(self, unit_name):
        '''
        Returns the number of turns until a unit falls off of the map.
        -1 is returned if the unit will not fall off the map.
        -2 if there is no unit on the space.
        -3 if the space does not exist.
        '''
        # Safety checks.
        if unit_name not in self.spaces.keys():
            return -3
        if self.spaces[unit_name].owner == "":
            return -2
        # Measure time.
        result = 1
        for s in self.spaces[unit_name].path:
            if s not in self.spaces.keys():
                return result
            result += 1
        # Looks like this will not fall off the map.
        return -1

    def all_that_will_fall(self):
        '''
        Returns a dictionary mapping units to the time unit they fall.
        '''
        result = dict()
        for s in self.spaces.keys():
            fall_number = self.time_until_fall(s)
            if fall_number >= 0:
                result[s] = fall_number
        return result

    def check_win(self):
        '''
        Returns winning team or 0 if the game is still in motion.
        '''
        # Produce a set of teams who have players not on the board.
        present_colors = set()
        for s in self.spaces.values():
            present_colors.add(s.owner)
        losing_teams = set()
        for p in self.players:
            if p != 0:
                if p.color not in present_colors:
                    losing_teams.add(p.team)
        # If no team has lost yet, continue.
        if len(losing_teams) == 0:
            return 0
        # If one team is missing a player, the other team wins.
        if len(losing_teams) == 1:
            return 3-losing_teams.pop()
        # If both teams have lost players, go to distance winner.
        return self.distance_winner

    def space_title(self, space_name):
        if space_name not in self.spaces.keys():
            return space_name + "(?)"
        poc = self.get_player_of_color(self.spaces[space_name].owner)
        if poc == 0:
            return space_name
        return space_name + "(" + poc.player_title() + ")"

    def units_of(self, player_name):
        '''
        Return all units owned by a given player.  Return 0 if the player does
        not exist.
        '''
        # Try to get player
        player = self.get_player_of_name(player_name)
        if player == 0:
            return 0
        # Get the people in the list
        result = []
        for s in self.spaces.keys():
            if self.spaces[s].owner == player.color:
                result.append(s)
        return result

    def prospects(self, player_name, breadth, depth):
        '''
        How good things look for a player.  This is a heuristic function for
        use in the AI.  Returns [prospects, next advised move from, to]
        In prospects, 1 = you win, 0 = you lose.
        '''
        pon = self.get_player_of_name(player_name)
        # Bail if the player is wrong.
        if pon == 0:
            return 0
        # Bail right now if the game is over.
        w = self.check_win()
        if w != 0:
            return [float(pon.team == w), "", ""]
        # Have you run out of depth to explore?  If so, guess how you're doing!
        if depth == 0:
            # Ratio of your smallest player to their smallest player.
            min_ally     = float('inf')
            min_opponent = float('inf')
            for p in self.players:
                if p != 0:
                    new_val = float(len(self.units_of(p.name)))
                    if p.team == pon.team:
                        min_ally = min(min_ally, new_val)
                    else:
                        min_opponent = min(min_opponent, new_val)
            return [min_ally / (min_opponent + min_ally), "", ""]
        # If you still have depth to explore, try some moves.
        best_move_from = ""
        best_move_to   = ""
        best_move_prospects = -1
        for m in range(breadth):
            # Create a board to test this.
            test_board = self.clone()
            # Decide move to try.
            if best_move_prospects == -1:
                # Always first try not moving.
                picked_move_from = ""
                picked_move_to   = ""
            else:
                # Execute random move.
                picked_move_from = random.choice(test_board.units_of(player_name))
                picked_move_to   = random.choice(test_board.spaces.keys())
            picked_move_prospects = -1
            test_board.set_destination(picked_move_from, picked_move_to)
            test_board.rotate_players()
            # Finish round cycle, including win/lose check.
            while test_board.current_player() == 0 or test_board.current_player().name != player_name:
                if test_board.current_player() == 0:
                    test_board.advance_units(False)
                    w = test_board.check_win()
                    if w != 0:
                        picked_move_prospects = float(pon.team == w)
                        break
                    test_board.update_distance_winner()
                else:
                    prosp = test_board.prospects(test_board.current_player().name, breadth, depth-1)
                    test_board.set_destination(prosp[1], prosp[2])
                test_board.rotate_players()
            # Now repeat your move.
            if picked_move_prospects == -1:
                picked_move_prospects = test_board.prospects(player_name, breadth, depth-1)[0]
            # Is this move the best you've seen so far?
            #if depth == 3: # for testing
            #    print picked_move_from + "->" + picked_move_to + " " + str(picked_move_prospects)
            if picked_move_prospects >= best_move_prospects:
                best_move_from      = picked_move_from
                best_move_to        = picked_move_to
                best_move_prospects = picked_move_prospects
        return [best_move_prospects, best_move_from, best_move_to]
