class space:
    def __init__(self, name):
        self.name = name
        self.edges = set()
        self.owner = ""
        self.path = []
    def connect(self, other_space):
        if self != other_space:
            self.edges.add(other_space)
            other_space.edges.add(self)
